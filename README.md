Parsing tool using **Spring Framework** for learning purposes.

**Target site:** [http://godville.net/](http://godville.net/)

**Object:** Parse all item and skill names for crossword solving. Crossword was published on daily basis on the project's title page.
