package ua.vydai.godville.parser;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.ObjectFactoryCreatingFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import ua.vydai.godville.parser.consumer.Consumer;
import ua.vydai.godville.parser.consumer.GodsConsumer;
import ua.vydai.godville.parser.phase.ExecutionPhase;
import ua.vydai.godville.parser.phase.ParseItems;
import ua.vydai.godville.parser.process.HCParserProcess;

@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:jdbc.properties" })
@ComponentScan({ "ua.vydai.godville.parser" })
public class ApplicationStarter
{
    private static final Logger LOG = LoggerFactory
            .getLogger(ApplicationStarter.class);

    @Resource(name = "executionPhases")
    private List<ExecutionPhase> phases;

    @Inject
    private Environment env;

    public ApplicationStarter()
    {
    }

    @Bean
    public List<ExecutionPhase> executionPhases()
    {
        return Arrays.asList(new ExecutionPhase[]{new ParseItems(webDriver(), dataConsumer())});
    }

    @Bean
    public WebDriver webDriver()
    {
        return new HtmlUnitDriver();
    }

    @Bean
    public Consumer dataConsumer()
    {
        return new GodsConsumer(4); // threads count #
    }

    /** Bean for parser implementation selection */
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public HCParserProcess parserProcess() {
        return new HCParserProcess();
    }

    @Bean
    public ObjectFactoryCreatingFactoryBean parserProcessFactory()
    {
        ObjectFactoryCreatingFactoryBean objectFactory = new ObjectFactoryCreatingFactoryBean();
        objectFactory.setTargetBeanName("parserProcess");
        return objectFactory;
    }

    /** JDBC data source setup */
    @Bean
    public DataSource restDataSource() {
       DriverManagerDataSource dataSource = new DriverManagerDataSource();
       dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
       dataSource.setUrl(env.getProperty("jdbc.databaseurl"));
       dataSource.setUsername(env.getProperty("jdbc.username"));
       dataSource.setPassword(env.getProperty("jdbc.password"));
       return dataSource;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
       return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    @Inject
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
       HibernateTransactionManager txManager = new HibernateTransactionManager();
       txManager.setSessionFactory(sessionFactory);
       return txManager;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
       LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
       sessionFactory.setDataSource(restDataSource());
       sessionFactory.setPackagesToScan(new String[] { "ua.vydai.godville.parser.persistence" });
       sessionFactory.setHibernateProperties(hibernateProperties());
       return sessionFactory;
    }

    private Properties hibernateProperties() {
        Properties p = new Properties();
        p.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        p.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
        p.setProperty("hibernate.connection.charSet", "UTF-8");
        p.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));
        p.setProperty("hibernate.connection.autocommit", "false");
        p.setProperty("hibernate.globally_quoted_identifiers", "true");
        return p;
     }

    public static void main(String[] args)
    {
        final ApplicationContext ctx = SpringApplication.run(ApplicationStarter.class, args);
        
//        System.out.println("Let's inspect the beans provided by Spring Boot:");
//        
//        String[] beanNames = ctx.getBeanDefinitionNames();
//        Arrays.sort(beanNames);
//        for (String beanName : beanNames) {
//            System.out.println(beanName);
//        }
        
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run()
            {
                ((ConfigurableApplicationContext) ctx).close();
                LOG.info("Application finished");
            }
        });
        ctx.getBean(ApplicationStarter.class).start();
    }

    public void start()
    {
        LOG.info("Application started");
        try
        {
            for (ExecutionPhase phase : phases)
            {
                phase.run();
            }
        }
        catch (RuntimeException e)
        {
            LOG.error("Application halted with exception: ", e);
            throw e;
        }
    }
}
