package ua.vydai.godville.parser.consumer;

import java.util.List;

import ua.vydai.godville.parser.persistence.Item;

public interface Consumer
{
    void consume(String name);

    List<Item> getItems();

    void start();

    void shutdown();
}
