package ua.vydai.godville.parser.consumer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;

import ua.vydai.godville.parser.persistence.Item;
import ua.vydai.godville.parser.process.ParserProcess;

public class GodsConsumer implements Consumer {
    private static final Logger LOG = LoggerFactory.getLogger(GodsConsumer.class);

    private final int threadsCount;

    private BlockingQueue<String> godNames;

    private ConcurrentLinkedQueue<Item> items;

    private ExecutorService pool;

    private List<ParserProcess> processes;

    @Inject
    private ObjectFactory<ParserProcess> processFactory;

    public GodsConsumer(int threadsCount) {
        this.threadsCount = threadsCount;
    }

    @Override
    public void consume(String name) {
        try {
            godNames.offer(name, 20, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOG.error("God name offer was interrupted!", e);
        }
    }

    @Override
    public List<Item> getItems() {
        final List<Item> result = new ArrayList<Item>();
        for (int i = items.size(); i > 0; i--) {
            result.add(items.remove());
        }
        return result;
    }

    @Override
    public void start() {
        this.godNames = new ArrayBlockingQueue<String>(threadsCount);
        this.items = new ConcurrentLinkedQueue<Item>();
        this.pool = Executors.newFixedThreadPool(threadsCount);
        this.processes = new ArrayList<ParserProcess>(threadsCount);
        for (int i = 0; i < threadsCount; i++) {
            ParserProcess process = processFactory.getObject();
            process.setInputSource(godNames);
            process.setOutputSource(items);
            processes.add(process);
            pool.execute(process);
        }
    }

    @Override
    public void shutdown() {
        for (ParserProcess process : processes) {
            process.shutdown();
        }
        pool.shutdown();
        try {
            pool.awaitTermination(20, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOG.error("Pool shutdown was interrupted!", e);
        }
    }
}
