package ua.vydai.godville.parser.persistence;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ItemBO {
    @Inject
    private ItemDAO dao;

    @Transactional
    public void save(Item item) {
        dao.save(item);
    }
}
