package ua.vydai.godville.parser.persistence;

import javax.inject.Inject;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ItemDAO {
    @Inject
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public void save(Item item) {
        getCurrentSession().merge(item);
    }
}
