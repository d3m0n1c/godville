package ua.vydai.godville.parser.persistence;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Table(schema = "godville")
@Entity(name = "Items")
public class Item {
    @Id
    @Type(type = "org.hibernate.type.StringClobType")
    @Column(nullable = false, unique = true)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
