package ua.vydai.godville.parser.process;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.vydai.godville.parser.persistence.Item;

public class HUParserProcess extends ParserProcess {
    private static final Logger LOG = LoggerFactory.getLogger(HUParserProcess.class);

    private final HtmlUnitDriver godBrowser;

    private boolean isRunning = true;

    public HUParserProcess() {
        this.godBrowser = new HtmlUnitDriver();
        this.godBrowser.setJavascriptEnabled(false);
    }

    @Override
    public void run() {
        try {
            while (isRunning || !getInputSource().isEmpty()) {
                String godName = getInputSource().poll(20, TimeUnit.SECONDS);
                if (godName == null) {
                    continue;
                }
                openGod(godName);
                parseItems();
            }
        } catch (InterruptedException e) {
            LOG.error("God name poll was interrupted!", e);
        } finally {
            close();
        }
    }

    public void shutdown() {
        isRunning = false;
    }

    private void openGod(String name) {
        godBrowser.get("http://godville.net/gods/" + name);
    }

    private void parseItems() {
        // parse equipment
        List<WebElement> equips = godBrowser.findElements(By.xpath("//*[@id='equipment']/tbody/tr/td[2]"));
        for (WebElement equip : equips) {
            String itemName = equip.getText();
            if ("пусто".equals(itemName)) {
                continue;
            }
            Item item = new Item();
            item.setName(itemName);
            getItemBO().save(item);
        }
        // parse skills
        List<WebElement> skills = godBrowser.findElements(By.xpath("//*[@id='column_2']/ul/li"));
        for (WebElement skill : skills) {
            String skillName = skill.getText().split(" \\d+-го уровня")[0];
            Item item = new Item();
            item.setName(skillName);
            getItemBO().save(item);
        }
        // parse pet
        try {
            WebElement pet = godBrowser.findElement(By.xpath("//*[@id='characteristics']/tbody/tr[td='Питомец']/td[@class='name']/a"));
            Item item = new Item();
            item.setName(pet.getText());
            getItemBO().save(item);
        } catch (NoSuchElementException e) {
        }
    }

    private void close() {
        godBrowser.close();
    }
}
