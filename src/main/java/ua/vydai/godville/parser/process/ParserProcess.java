package ua.vydai.godville.parser.process;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.inject.Inject;

import ua.vydai.godville.parser.persistence.Item;
import ua.vydai.godville.parser.persistence.ItemBO;

public abstract class ParserProcess implements Runnable
{
    @Inject
    private ItemBO itemBO;

    private BlockingQueue<String> inputSource;

    private ConcurrentLinkedQueue<Item> outputSource;

    public abstract void shutdown();

    public ItemBO getItemBO()
    {
        return itemBO;
    }

    public BlockingQueue<String> getInputSource()
    {
        return inputSource;
    }

    public void setInputSource(BlockingQueue<String> inputSource)
    {
        this.inputSource = inputSource;
    }

    public ConcurrentLinkedQueue<Item> getOutputSource()
    {
        return outputSource;
    }

    public void setOutputSource(ConcurrentLinkedQueue<Item> outputSource)
    {
        this.outputSource = outputSource;
    }

}
