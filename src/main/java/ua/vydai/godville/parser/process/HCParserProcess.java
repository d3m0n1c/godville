package ua.vydai.godville.parser.process;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.vydai.godville.parser.persistence.Item;

public class HCParserProcess extends ParserProcess {
    private static final Logger LOG = LoggerFactory.getLogger(HCParserProcess.class);

    private static final Pattern EQUIPMENT = Pattern.compile(".*</td>\\s*<td class=\"name\">(.*?)</td>\\s*<td");

    private static final Pattern SKILL = Pattern.compile(".*<ul class=\"b_list\">\\s*<li>(.*?)<span");

    private static final Pattern PET = Pattern.compile(".*<td class=\"label\">Питомец</td>\\s*<td class=\"name\"><a.*>(.*?)</a>");

    private final CloseableHttpClient httpClient;

    private boolean isRunning = true;

    public HCParserProcess() {
        this.httpClient = HttpClients.createDefault();
    }

    @Override
    public void run() {
        try {
            while (isRunning || !getInputSource().isEmpty()) {
                String godName = getInputSource().poll(20, TimeUnit.SECONDS);
                if (godName == null) {
                    continue;
                }
                String pageText = getPage(godName);
                if (pageText != null) {
                    parseItems(pageText);
                } else {
                    LOG.error("Null page for god [{}] can't be parsed!", godName);
                }
            }
        } catch (InterruptedException e) {
            LOG.error("God name poll was interrupted!", e);
        } finally {
            close();
        }
    }

    private void parseItems(String pageText) {
        // parse equipment names
        Matcher equipMatcher = EQUIPMENT.matcher(pageText);
        while (equipMatcher.find()) {
            String itemName = equipMatcher.group(1);
            if ("пусто".equals(itemName)) {
                continue;
            }
            Item item = new Item();
            item.setName(itemName);
            getItemBO().save(item);
        }
        // parse skills
        Matcher skillMatcher = SKILL.matcher(pageText);
        while (skillMatcher.find()) {
            Item item = new Item();
            item.setName(skillMatcher.group(1));
            getItemBO().save(item);
        }
        // parse pet names
        Matcher petMatcher = PET.matcher(pageText);
        while (petMatcher.find()) {
            Item item = new Item();
            item.setName(petMatcher.group(1));
            getItemBO().save(item);
        }
    }

    private String getPage(String name) {
        String encodedName;
        try
        {
            encodedName = URLEncoder.encode(name, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException(e);
        }
        final HttpGet getRequest = new HttpGet("http://godville.net/gods/" + encodedName);
        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(
                    final HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    LOG.warn("Unexpected response status: {}, URI: {}", status, getRequest.getURI());
                }
                return null;
            }
        };
        try {
            return httpClient.execute(getRequest, responseHandler);
        } catch (ClientProtocolException e) {
            LOG.error("Error on client protocol!", e);
        } catch (IOException e) {
            LOG.error("Error on response reading!", e);
        }
        return null;
    }

    private void close() {
        try {
            httpClient.close();
        } catch (IOException e) {
            LOG.error("Error on client close!", e);
        }
    }

    @Override
    public void shutdown() {
        isRunning = false;
    }
}
