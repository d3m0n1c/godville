package ua.vydai.godville.parser.phase;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import ua.vydai.godville.parser.consumer.Consumer;

public class ParseItems extends WebPhase {
    private final Consumer consumer;

    public ParseItems(WebDriver driver, Consumer consumer) {
        super(driver);
        this.consumer = consumer;
    }

    @Override
    protected String getBaseURL() {
        return "http://godville.net";
    }

    @Override
    protected void execute() {
        consumer.start();
        login();
        navigate("/pantheon/show/mastery");
        goToPage(1);
        boolean hasNextPage = true;
        do {
            log.info("Current page: " + getCurrentPage());
            List<WebElement> elements = driver().findElements(By.xpath("//*[@id='pant_tbl']/tbody/tr/td[3]"));
            log.info("Names count on the page: " + elements.size());
            final List<String> godNames = new ArrayList<String>(100);
            for (WebElement element : elements) {
                godNames.add(element.getText());
            }
            // go to the next page and wait till gods will be parsed
            hasNextPage = nextPage();
            // parse gods
            for (final String godName : godNames) {
                consumer.consume(godName);
            }
        } while (hasNextPage);
        // shutdown consumer and wait for completion
        consumer.shutdown();
    }

    private void goToPage(int page) {
        if (driver() instanceof JavascriptExecutor) {
            JavascriptExecutor jsExecutorDriver = (JavascriptExecutor) driver();
            jsExecutorDriver
                    .executeScript(
                            "new Ajax.Request('/pantheon/update_data?page={}&type=by_mast', {asynchronous:true, evalScripts:true, onSuccess:function(request){Element.hide('spinner_pant')}});",
                            page);
        } else {
            log.error("Can't change page for driver that doesn't implement JavascriptExecutor");
        }
    }

    private boolean nextPage() {
        WebElement link = driver().findElement(By.xpath("//*[@class='pagination']/a[last()]"));
        if ("  →".equals(link.getText())) {
            link.click();
            return true;
        }
        return false;
    }

    private String getCurrentPage() {
        return driver().findElement(By.xpath("//*[@class='current']")).getText();
    }

    private void login() {
        navigate("");
        driver().findElement(By.id("username")).sendKeys("Reklaw");
        driver().findElement(By.id("password")).sendKeys("parser");
        driver().findElement(By.className("input_btn")).click();
    }
}
