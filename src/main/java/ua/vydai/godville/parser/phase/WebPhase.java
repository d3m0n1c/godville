package ua.vydai.godville.parser.phase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ua.vydai.godville.parser.persistence.Item;

public abstract class WebPhase extends ExecutionPhase {
    private WebDriver driver;

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    public WebPhase(WebDriver driver) {
        this.driver = driver;
    }

    protected WebDriver driver() {
        return driver;
    }

    public final void run() {
        String scenarioName = this.getClass().getSimpleName();
        log.info("Web phase '{}' started", scenarioName);
        WebDriver driver = driver();
        if (driver instanceof HtmlUnitDriver) {
            HtmlUnitDriver htmlUnitDriver = (HtmlUnitDriver) driver;
            htmlUnitDriver.setJavascriptEnabled(true);
        }
        try {
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            execute();
        } finally {
            driver.quit();
        }
        log.info("Web phase '{}' finished", scenarioName);
    }

    protected void save(Item item) {
        getItemBO().save(item);
    }

    protected void navigate(String url) {
        driver().get(getBaseURL() + url);
    }

    protected WebElement findElement(By by) {
        return driver().findElement(by);
    }

    protected abstract String getBaseURL();

    protected abstract void execute();
}
