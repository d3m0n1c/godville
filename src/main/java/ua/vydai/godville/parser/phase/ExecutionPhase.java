package ua.vydai.godville.parser.phase;

import javax.inject.Inject;

import ua.vydai.godville.parser.persistence.ItemBO;

public abstract class ExecutionPhase implements Runnable {
    @Inject
    private ItemBO itemBO;

    protected ItemBO getItemBO() {
        return itemBO;
    }
}
